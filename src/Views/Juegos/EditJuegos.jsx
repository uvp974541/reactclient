import React from 'react'
import FormJuegos from '../../Components/FormJuegos'
import { useParams } from 'react-router-dom'

function EditJuegos() {
  const {id}=useParams();
  return (
    <FormJuegos id={id} title='Editar Juego'>

    </FormJuegos>
  )
}

export default EditJuegos