import React, { useEffect, useState } from 'react'
import DivAdd from '../../Components/DivAdd'
import { DivTable } from '../../Components/DivTable'
import { Link } from 'react-router-dom'
import { confirmation,sendRequest } from '../../functions'

const Juegos = () => {
  const [juegos,SetJuegos] = useState([]);
  const [classLoad,setClassLoad] = useState('');
  const [classTable,setClassTable]= useState('d-none');
  useEffect( ()=>{
    getJuegos();
  },[])
  const getJuegos= async()=>{
    const res= await sendRequest ('GET','','/api/juegos','');
    SetJuegos(res);
    setClassTable('');
    setClassLoad('d-none');
  }
  const deleteJuego = (id,titulo)=>{
    confirmation(titulo,'/api/juegos/'+id,'');
  }
  return (
    <div className='container-fluid'>
      <DivAdd>
        <Link to='/createjuegos' className='btn btn-dark'>
          <i className='fa-solid fa-circle-plus'>Añadir</i>
        </Link>
      </DivAdd>
      <DivTable col='6' off='3' classLoad={classLoad}>
        <table className='table table-bordered'>
          <thead>
            <tr>
              <th>#</th>
              <th>Título</th>
              <th>Género</th>
              <th>Desarrolladora</th>
              <th>Fecha</th>
              <th></th>
              <th></th>
            </tr>
          </thead>
          <tbody className='table-group-divider'>
            {juegos.map((row,i)=>(
              <tr key={row.id}>
                <td>{(i+1)}</td>
                <td>{row.titulo}</td>
                <td>{row.genero}</td>
                <td>{row.desarrolladora}</td>
                <td>{row.fecha}</td>
                <td>
                  <Link to={'/editjuegos/'+row.idJuegos} className='btn btn-warning'>
                    <i className='fa-solid fa-edit'></i>
                  </Link>
                </td>
                <td>
                  <button className='btn btn-danger'
                  onClick={()=>deleteJuego(row.idJuegos,row.titulo)}>
                    <i className='fa-solid fa-trash'></i>
                  </button>
                </td>                
              </tr>
            ))}
          </tbody>
        </table>
      </DivTable>
    </div>
  )
}

export default Juegos