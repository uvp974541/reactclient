import React, { useEffect, useState } from 'react'
import DivAdd from '../../Components/DivAdd'
import { DivTable } from '../../Components/DivTable'
import { Link } from 'react-router-dom'
import { confirmation,sendRequest } from '../../functions'

function Sucursales() {
  const [sucursales,setSucursales] = useState([]);
  const [classLoad,setClassLoad] = useState('');
  const [classTable,setClassTable]= useState('d-none');
  useEffect( ()=>{
    getSucursales();
  },[])
  const getSucursales= async()=>{
    const res= await sendRequest ('GET','','/api/sucursales','');
    setSucursales(res);
    setClassTable('');
    setClassLoad('d-none');
  }
  const deleteSucursales = (id,titulo)=>{
    confirmation(titulo,('/api/sucursales/'+id,'/'));
  }
  return (
    <div className='container-fluid'>
      <DivAdd>
        <Link to='/createsucursal' className='btn btn-dark'>
          <i className='fa-solid fa-circle-plus'>Añadir</i>
        </Link>
      </DivAdd>
      <DivTable col='6' off='3' classLoad={classLoad}>
        <table className='table table-bordered'>
          <thead>
            <tr>
              <th>#</th>
              <th>Nombre</th>
              <th>Ubicación</th>
              <th></th>
              <th></th>
            </tr>
          </thead>
          <tbody className='table-group-divider'>
            {sucursales.map((row,i)=>(
              <tr key={row.id}>
                <td>{(i+1)}</td>
                <td>{row.nombre_sucursal}</td>
                <td>{row.ubicacion}</td>

                <td>
                  <Link to={'/editsucursal/'+row.idSucursal} className='btn btn-warning'>
                    <i className='fa-solid fa-edit'></i>
                  </Link>
                </td>
                <td>
                  <button className='btn btn-danger'
                  onClick={()=>deleteSucursales(row.id,row.nombre_sucursal)}>
                    <i className='fa-solid fa-trash'></i>
                  </button>
                </td>                
              </tr>
            ))}
          </tbody>
        </table>
      </DivTable>
    </div>
  )
}

export default Sucursales