import React from 'react'
import FormSucursal from '../../Components/FormSucursal'
import { useParams } from 'react-router-dom'

function EditSucursal() {
  const {id}=useParams();
  return (
    <FormSucursal id={id} title='Editar Sucursal'>

    </FormSucursal>
  )
}

export default EditSucursal