import React,{useEffect,useState,useRef} from 'react'
import DivAdd from '../../Components/DivAdd';
import { DivTable } from '../../Components/DivTable'
import { confirmation,sendRequest } from '../../functions'

const Ventas = () => {
  const [ventas,setVentas]=useState([]);
  const [id,setId]=useState('');
  const [clienteId,setClienteId]=useState('');
  const [clientes, setClientes]=useState([]);
  const [juegoId,setJuegoID]=useState('');
  const [juegos, setJuegos]=useState([]);
  const [sucursalId,setSucursalID]=useState('');
  const [sucursales, setSucursales]=useState([]);
  const [classLoad,setClassLoad]=useState('');
  const [classTable,setClassTable]=useState('d-none');
  const close = useRef();
  let method='';
  let url='';
  let dir='';
  useEffect(()=>{
    getVentas();

  },[]);
  const getVentas= async()=>{
    const res= await sendRequest ('GET','','/api/ventas','');
    setVentas(res);
    setClassTable('');
    setClassLoad('d-none');
  }

 
  const deleteVentas =(id)=>{
    confirmation(id,'/api/ventas/'+id,'ventas');
  }
  const clear=()=>{
    setClienteId('');
    setSucursalID('');
    setJuegoID('');
  }
  const openModal = (op,cli,jue,suc)=>{
    clear();
    setOperation(op);
    if(op==1){
      setTitle('Agregar venta');
    }
    else{
      setTitle('Actualizar venta');
      setClienteId(cli);
      setJuegoID(jue);
      setSucursalID(suc)
    }
    const save = async(e)=>{
      e.preventDefault();
      if(operation==1){
        method = 'POST';
        url='/api/ventas'
      }
      else{
        method='PUT';
        url='/api/ventas/id'
      }
      const form= {Clientes_idClientes:clienteId,Juegos_IdJuegos:juegoId,Sucursal_idSucursal:sucursalId}
      const res = await sendRequest(method,form,url,'');
      if (method=PUT && res.status == true){
        close.current.click();
      }
      if(res.status==true){
        clear();
        getVentas();
      }

    }
  }

  return (
    <div className='container-fluid'>
      <DivAdd>
        <button className='btn btn-sark' datatype=''>
          
        </button>
      </DivAdd>
      <DivTable col='6' off='3' classLoad={classLoad}>
        <table className='table table-bordered'>
          <thead>
            <tr>
              <th>#</th>
              <th>Nombre</th>
              <th>Ubicación</th>
              <th></th>
              <th></th>
            </tr>
          </thead>
          <tbody className='table-group-divider'>
            {ventas.map((row,i)=>(
              <tr key={row.id}>
                <td>{(i+1)}</td>
                <td>{row.Clientes_idClientes.nombre_cliente}</td>
                <td>{row.Juegos_idJuegos}</td>

                <td>
                  <Link to={'/editventas/'+row.idVentas} className='btn btn-warning'>
                    <i className='fa-solid fa-edit'></i>
                  </Link>
                </td>
                <td>
                  <button className='btn btn-danger'
                  onClick={()=>deleteVentas(row.id,row.id)}>
                    <i className='fa-solid fa-trash'></i>
                  </button>
                </td>                
              </tr>
            ))}
          </tbody>
        </table>
      </DivTable>
    </div>
  )
}

export default Ventas