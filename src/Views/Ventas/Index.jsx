import React, { useEffect, useState } from 'react'
import DivAdd from '../../Components/DivAdd'
import { DivTable } from '../../Components/DivTable'
import { Link } from 'react-router-dom'
import { confirmation,sendRequest } from '../../functions'

const Ventas = () => {
  const [ventas,setVentas] = useState([]);
  const [classLoad,setClassLoad] = useState('');
  const [classTable,setClassTable]= useState('d-none');
  useEffect( ()=>{
    getVentas();
  },[])
  const getVentas= async()=>{
    const res= await sendRequest ('GET','','/api/ventas','');
    setVentas(res);
    setClassTable('');
    setClassLoad('d-none');
  }
  const deleteVentas = (id,titulo)=>{
    confirmation(titulo,'api/ventas/'+id,'/');
    getVentas();
  }
  return (
    <div className='container-fluid'>
      <DivAdd>
        <Link to='/createventas' className='btn btn-dark'>
          <i className='fa-solid fa-circle-plus'>Añadir</i>
        </Link>
      </DivAdd>
      <DivTable col='6' off='3' classLoad={classLoad}>
        <table className='table table-bordered'>
          <thead>
            <tr>
              <th>#</th>
              <th>Cliente</th>
              <th>Juego</th>
              <th>Sucursal</th>
              <th></th>
              <th></th>
            </tr>
          </thead>
          <tbody className='table-group-divider'>
            {ventas.map((row,i)=>(
              <tr key={row.id}>
                <td>{(i+1)}</td>
                <td>{row.cliente.nombre_cliente}</td>
                <td>{row.juego.titulo}</td>
                <td>{row.sucursal.nombre_sucursal}</td>
                <td>
                  <Link to={'/editventas/'+row.idVentas} className='btn btn-warning'>
                    <i className='fa-solid fa-edit'></i>
                  </Link>
                </td>
                <td>
                  <button className='btn btn-danger'
                  onClick={()=>deleteVentas(row.idVentas,'esta venta')}>
                    <i className='fa-solid fa-trash'></i>
                  </button>
                </td>                
              </tr>
            ))}
          </tbody>
        </table>
      </DivTable>
    </div>
  )
}

export default Ventas