import React from 'react'
import FormClientes from '../../Components/FormClientes';
import { useParams } from 'react-router-dom'

function EditClientes() {
  const {id}=useParams();
  return (
    <FormClientes id={id} title='Editar Clientes'>

    </FormClientes>
  )
}

export default EditClientes