import React, { useEffect, useState } from 'react'
import DivAdd from '../../Components/DivAdd'
import { DivTable } from '../../Components/DivTable'
import { Link } from 'react-router-dom'
import { confirmation,sendRequest } from '../../functions'

const Clientes = () => {
  const [clientes,setClientes] = useState([]);
  const [classLoad,setClassLoad] = useState('');
  const [classTable,setClassTable]= useState('d-none');
  useEffect( ()=>{
    getClientes();
  },[])
  const getClientes= async()=>{
    const res= await sendRequest ('GET','','/api/clientes','');
    setClientes(res);
    setClassTable('');
    setClassLoad('d-none');
  }
  const deleteCliente = (id,nombre)=>{
    confirmation(nombre,('/api/clientes/'+id,'/'));
  }
  return (
    <div className='container-fluid'>
      <DivAdd>
        <Link to='/createclientes' className='btn btn-dark'>
          <i className='fa-solid fa-circle-plus'>Añadir</i>
        </Link>
      </DivAdd>
      <DivTable col='6' off='3' classLoad={classLoad}>
        <table className='table table-bordered'>
          <thead>
            <tr>
              <th>#</th>
              <th>Nombre</th>
              <th>Correo</th>
              <th>Teléfono</th>
              <th></th>
              <th></th>
            </tr>
          </thead>
          <tbody className='table-group-divider'>
            {clientes.map((row,i)=>(
              <tr key={row.id}>
                <td>{(i+1)}</td>
                <td>{row.nombre_cliente}</td>
                <td>{row.correo_cliente}</td>
                <td>{row.telefono_cliente}</td>
                <td>
                  <Link to={'/editclientes/'+row.idClientes} className='btn btn-warning'>
                    <i className='fa-solid fa-edit'></i>
                  </Link>
                </td>
                <td>
                  <button className='btn btn-danger'
                  onClick={()=>deleteCliente(row.id,row.nombre_cliente)}>
                    <i className='fa-solid fa-trash'></i>
                  </button>
                </td>                
              </tr>
            ))}
          </tbody>
        </table>
      </DivTable>
    </div>
  )
}

export default Clientes