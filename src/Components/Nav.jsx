import { Link, useNavigate } from 'react-router-dom'
import React from 'react'
import storage from '../Storage/storage';

const Nav = () => {
  const go = useNavigate();
  const logout = async()=>{
    storage.remove('authToken');
    storage.remove('authUser');
    await axios.get('/api/auth/logout',storage.get('authToken'));
    go('/login')
  }
  return (
    <nav className= 'navbar navbar-expand-lg navbar-white bg-danger'>
      <div className='container-fluid'>
        <a className='navbar-brand'>Tienda de juegos</a>
        <button className='navbar-toggler' type='button' data-bs-toggle='collapse'
        data-bs-targe='#nav' aria-controls='navbarSupportedContent'>
          <span className='navbar-toggler-icon'></span>
        </button>
      </div>
      {storage.get('authUser')?(
        <div className='collapse navbar-collapse' id='nav'>
        <ul className='navbar-nav mx-auto mb-2'>
        <li className='nav-item px-lg-5'>
            {storage.get('authUser').email}
          </li >
          <li className='nav-item px-lg-5'>
            <Link to='/' className='nav-link'>Ventas</Link>
          </li>
          <li className='nav-item px-lg-5'>
            <Link to='/juegos' className='nav-link'>Juegos</Link>
          </li>
          <li className='nav-item px-lg-5'>
            <Link to='/clientes' className='nav-link'>Clientes</Link>
          </li>
          <li className='nav-item px-lg-5'>
            <Link to='/graphic' className='nav-link'>Gráfica</Link>
          </li>
          <li className='nav-item px-lg-5'>
            <Link to='/sucursales' className='nav-link'>Sucursales</Link>
          </li>
        </ul>
        <ul className='navbar-nav mx-auto mb-2'>
          <li className='nab-item px-lg-5'>
            <button className='btn btn-black' onClick={logout}>Logout</button>
          </li>
        </ul>
      </div>
      ):''}
      

    </nav>
  )
}

export default Nav