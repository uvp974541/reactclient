import React,{useState,useEffect,useRef} from "react";
import { sendRequest } from "../functions";
import DivInput from "./DivInput";
import DivSelect from "./DivSelect";

const FormVentas = (params) => {
    
    const [cliente,setCliente]=useState('');
    const [juego,setJuego]=useState('');
    const [sucursal, setSucursal]=useState('');
    const [juegos,setJuegos]=useState([]);
    const [clientes, setClientes]=useState([]);
    const [sucursales,setSucursales]=useState([]);

    

    let method = 'POST';
    let url = '/api/ventas';
    let redirect = '';
    useEffect(()=>{
        getVentas();
        getJuegos();
        getSucursales();
        getClientes();
    },[]);
    
    const getVentas= async()=>{
        if(params.id !== null){
            const res= await sendRequest('GET','',(url+'/'+params.id));
            setCliente(res.cliente.idClientes);
            setJuego(res.juego.idJuegos);
            setSucursal(res.sucursal.idSucursal)
        }
      }
    const getJuegos= async()=>{
        const res= await sendRequest('GET','','/api/juegos','');
        setJuegos(res);
    }
    const getClientes= async()=>{
        const res= await sendRequest('GET','','/api/clientes','');
        setClientes(res);
        
    } 
    const getSucursales= async()=>{
        const res= await sendRequest('GET','','/api/sucursales','');
        setSucursales(res);
    }    
    const save = async(e)=>{
        e.preventDefault();
        if(params.id !==null){
            method='PUT';
            url = 'api/ventas/'+params.id;
            redirect = '/';
            
        }
        const res = await sendRequest(method,{Clientes_idClientes:cliente,Juegos_idJuegos:juego,Sucursal_idSucursal:sucursal},url,redirect);
        if(method== 'POST'&& res.status==true){
            redirect = '/';
        }
    }
  return (
    <div className="container-fluid">
        <div className="row mt-5">
            <div className="col-md-4 offset-md-4">
                <div className="card border border-info">
                    <div className="card-header bg-info border border-info">
                        {params.title}
                    </div>
                    <div className="card-body">
                        <form onSubmit={save}>

                        <div className='input-group mb-3'>
                         <span className='input-group-text'>
                            <i className='fa-solid fa-user'></i>
                        </span>
                          <select  className='form-select' value={cliente}
                          required='required'
                          onChange={(e) => setCliente(e.target.value)} >
                              {clientes.map((op)=>(
                             <option value={op.idClientes} key={op.idClientes}>{op.nombre_cliente}</option>
                            ))}
                         </select>
                        </div>   

                        <div className='input-group mb-3'>
                         <span className='input-group-text'>
                            <i className='fa-solid fa-user'></i>
                        </span>
                          <select  className='form-select' value={juego}
                          required='required'
                          onChange={(e) => setJuego(e.target.value)} >
                              {juegos.map((op)=>(
                             <option value={op.idJuegos} key={op.idJuegos}>{op.titulo}</option>
                            ))}
                         </select>
                        </div>      

                        <div className='input-group mb-3'>
                         <span className='input-group-text'>
                            <i className='fa-solid fa-user'></i>
                        </span>
                          <select  className='form-select' value={sucursal}
                          required='required'
                          onChange={(e) => setSucursal(e.target.value)} >
                              {sucursales.map((op)=>(
                             <option value={op.idSucursal} key={op.idClientes}>{op.nombre_sucursal}</option>
                            ))}
                         </select>
                        </div>                          

                            <div className="d-grid col-10 mx-auto">
                                <button className="btn btn-dark">
                                    <i className="fa-solid fa-save"> Guardar</i>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
  )
}

export default FormVentas

