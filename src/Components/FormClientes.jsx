import React,{useState,useEffect,useRef} from "react";
import { sendRequest } from "../functions";
import DivInput from "./DivInput";

const FormClientes = (params) => {
    const [nombre,setNombre]=useState('');
    const [email,setEmail]=useState('');
    const [telefono,setTelefono]=useState('');
    
    const NombreInput = useRef();
    const EmailInput = useRef();
    const telefonoInput = useRef();

    let method = 'POST';
    let url = '/api/clientes';
    let redirect = '';
    useEffect(()=>{
        NombreInput.current.focus();
        getClientes();
        
    },[]);
    const getClientes = async()=>{
        if(params.id !== null){
            const res= await sendRequest('GET','',(url+'/'+params.id));
            setNombre(res.nombre_cliente);
            setEmail(res.email_cliente);
            setTelefono(res.telefono_cliente);
        }
    }
    const save = async(e)=>{
        e.preventDefault();
        if(params.id !==null){
            method='PUT';
            url = 'api/clientes/'+params.id;
            redirect = '/clientes';
            
        }
        const res = await sendRequest(method,{nombre_cliente:nombre,email_cliente:email,telefono_cliente:telefono},url,redirect);
        if(method== 'POST'&& res.status==true){
            setNombre('');
            setEmail('');
            setTelefono('');
            redirect = '/clientes';
        }
    }
  return (
    <div className="container-fluid">
        <div className="row mt-5">
            <div className="col-md-4 offset-md-4">
                <div className="card border border-info">
                    <div className="card-header bg-info border border-info">
                        {params.title}
                    </div>
                    <div className="card-body">
                        <form onSubmit={save}>
                            <DivInput type='text'
                            value={nombre} className='form-control'
                            placeholder='Nombre' required='required'
                            ref={NombreInput}
                            handleChange={(e)=>setNombre(e.target.value)}/>

                            <DivInput type='text'
                            value={email} className='form-control'
                            placeholder='E-mail' required='required'
                            ref={EmailInput}
                            handleChange={(e)=>setEmail(e.target.value)}/>

                            <DivInput type='text'
                            value={telefono} className='form-control'
                            placeholder='Telefono' required='required'
                            ref={telefonoInput}
                            handleChange={(e)=>setTelefono(e.target.value)}/>

                            <div className="d-grid col-10 mx-auto">
                                <button className="btn btn-dark">
                                    <i className="fa-solid fa-save"> Guardar</i>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
  )
}

export default FormClientes

