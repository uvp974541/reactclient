import React,{useState,useEffect,useRef} from "react";
import { sendRequest } from "../functions";
import DivInput from "./DivInput";

const FormSucursal = (params) => {
    const [nombre,setNombre]=useState('');
    const [ubicacion,setUbicacion]=useState('');
    
    const NombreInput = useRef();
    const UbicacionInput = useRef();

    let method = 'POST';
    let url = '/api/sucursales';
    let redirect = '';
    useEffect(()=>{
        NombreInput.current.focus();
        getSucursales();
        
    },[]);
    const getSucursales = async()=>{
        if(params.id !== null){
            const res= await sendRequest('GET','',(url+'/'+params.id));
            setNombre(res.nombre_sucursal);
            setUbicacion(res.ubicacion);
        }
    }
    const save = async(e)=>{
        e.preventDefault();
        if(params.id !==null){
            method='PUT';
            url = 'api/sucursales/'+params.id;
            redirect = '/sucursales';
            
        }
        const res = await sendRequest(method,{nombre_sucursal:nombre,ubicacion:ubicacion},url,redirect);
        if(method== 'POST'&& res.status==true){
            setNombre('');
            setUbicacion('');
            redirect = '/sucursales';
        }
    }
  return (
    <div className="container-fluid">
        <div className="row mt-5">
            <div className="col-md-4 offset-md-4">
                <div className="card border border-info">
                    <div className="card-header bg-info border border-info">
                        {params.title}
                    </div>
                    <div className="card-body">
                        <form onSubmit={save}>
                            <DivInput type='text'
                            value={nombre} className='form-control'
                            placeholder='Nombre' required='required'
                            ref={NombreInput}
                            handleChange={(e)=>setNombre(e.target.value)}/>

                            <DivInput type='text'
                            value={ubicacion} className='form-control'
                            placeholder='Ubicación' required='required'
                            ref={UbicacionInput}
                            handleChange={(e)=>setUbicacion(e.target.value)}/>

                            <div className="d-grid col-10 mx-auto">
                                <button className="btn btn-dark">
                                    <i className="fa-solid fa-save"> Guardar</i>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
  )
}

export default FormSucursal

