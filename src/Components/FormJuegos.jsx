import React,{useState,useEffect,useRef} from "react";
import { sendRequest } from "../functions";
import DivInput from "./DivInput";

const FormJuegos = (params) => {
    const [titulo,setTitulo]=useState('');
    const [genero,setGenero]=useState('');
    const [desarrolladora,setDesarrolladora]=useState('');
    const [fecha, setFecha]=useState('');
    
    const TituloInput = useRef();
    const GeneroInput = useRef();
    const DesarrolladoraInput=useRef();
    const fechaInput=useRef();

    let method = 'POST';
    let url = '/api/juegos';
    let redirect = '';
    useEffect(()=>{
        TituloInput.current.focus();
        getJuegos();
        
    },[]);
    const getJuegos = async()=>{
        if(params.id !== null){
            const res= await sendRequest('GET','',(url+'/'+params.id));
            setTitulo(res.titulo);
            setGenero(res.genero);
            setDesarrolladora(res.desarrolladora);
            setFecha(res.fecha);
        }
    }
    const save = async(e)=>{
        e.preventDefault();
        if(params.id !==null){
            method='PUT';
            url = 'api/juegos/'+params.id;
            redirect = '/';
            
        }
        const res = await sendRequest(method,{titulo:titulo,genero:genero,desarrolladora:desarrolladora,fecha:fecha},url,redirect);
        if(method== 'POST'&& res.status==true){
            setTitulo('');
            setGenero('');
            setDesarrolladora('');
            setFecha('');
        }
    }
  return (
    <div className="container-fluid">
        <div className="row mt-5">
            <div className="col-md-4 offset-md-4">
                <div className="card border border-info">
                    <div className="card-header bg-info border border-info">
                        {params.title}
                    </div>
                    <div className="card-body">
                        <form onSubmit={save}>
                            <DivInput type='text'
                            value={titulo} className='form-control'
                            placeholder='Título' required='required'
                            ref={TituloInput}
                            handleChange={(e)=>setTitulo(e.target.value)}/>

                            <DivInput type='text'
                            value={genero} className='form-control'
                            placeholder='Género' required='required'
                            ref={GeneroInput}
                            handleChange={(e)=>setGenero(e.target.value)}/>

                            <DivInput type='text'
                            value={desarrolladora} className='form-control'
                            placeholder='Desarrolladora' required='required'
                            ref={DesarrolladoraInput}
                            handleChange={(e)=>setDesarrolladora(e.target.value)}/>

                            <DivInput type='text'
                            value={fecha} className='form-control'
                            required='required'
                            ref={fechaInput}
                            handleChange={(e)=>setFecha(e.target.value)}/>

                            <div className="d-grid col-10 mx-auto">
                                <button className="btn btn-dark">
                                    <i className="fa-solid fa-save"> Guardar</i>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
  )
}

export default FormJuegos

