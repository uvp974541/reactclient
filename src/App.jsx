import { BrowserRouter,Routes,Route } from "react-router-dom"
import Nav from "./Components/Nav"
import Login from "./Views/Login"
import Register from "./Views/Register"

import ProtectedRoutes from "./Components/ProtectedRoutes"

import Ventas from "./Views/Ventas/Index"
import CreateVentas from "./Views/Ventas/CreateVentas"
import EditVentas from "./Views/Ventas/EditVentas"

import Juegos from "./Views/Juegos/Index"
import EditJuegos from "./Views/Juegos/EditJuegos"
import CreateJuegos from "./Views/Juegos/CreateJuegos"

import Sucursales from "./Views/Sucursales/Index"
import CreateSucursal from "./Views/Sucursales/CreateSucursales"
import EditSucursal from "./Views/Sucursales/EditSucursales"

import Clientes from "./Views/Clientes/Index"
import CreateClientes from "./Views/Clientes/CreateClientes"
import EditClientes from "./Views/Clientes/EditClientes"


import Graphic from "./Views/Juegos/Graphic"


function App() {

  return (
    <BrowserRouter>
    <Nav/>
      <Routes>
        <Route path="/login" element={<Login/>}/>
        <Route path="/register" element={<Register/>}/>
        <Route element={<ProtectedRoutes/>}>
          
          <Route path="/" element={<Ventas/>}/>
          <Route path="/createventas" element={<CreateVentas/>}/>
          <Route path="/editventas/:id" element={<EditVentas/>}/>
          
          <Route path="/juegos" element={<Juegos/>}/>
          <Route path="/createjuegos" element={<CreateJuegos/>}/>
          <Route path="/editjuegos/:id" element={<EditJuegos/>}/>
          
          <Route path="/sucursales" element={<Sucursales/>}/>
          <Route path="/createsucursal" element={<CreateSucursal/>}/>
          <Route path="/editsucursal/:id" element={<EditSucursal/>}/>  
                  
          <Route path="/clientes" element={<Clientes/>}/>
          <Route path="/editclientes/:id" element={<EditClientes/>}/>
          <Route path="/createclientes" element={<CreateClientes/>}/>
          <Route path="/graphic" element={<Graphic/>}/>
        </Route>
      </Routes>
    </BrowserRouter>  
  )
}

export default App
